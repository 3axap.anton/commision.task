<?php

declare(strict_types=1);

namespace App\Tests\Factory;

use App\Factory\Currency\CurrencyFactory;
use App\Model\Currency;
use App\Service\ParseCurrency\ParseCurrencyService;
use App\Tests\BasePhpUnit;

class CurrencyFactoryTest extends BasePhpUnit
{
    private CurrencyFactory $currencyFactory;

    public function setUp(): void
    {
        parent::setUp();

        $parseCurrencyServiceMock = $this->createMock(ParseCurrencyService::class);
        $parseCurrencyServiceMock->method('getRates')->willReturn([
            'EUR' => '1',
            'USD' => '1.1497',
            'JPY' => '129.53',
        ]);

        $this->currencyFactory = new CurrencyFactory(
            [
                'EUR' => 2,
                'USD' => 2,
                'JPY' => 0,
            ],
            $parseCurrencyServiceMock
        );
    }

    /**
     * @dataProvider buildDataProvider
     */
    public function testBuild(
        string $currency
    ): void {
        $currency = $this->currencyFactory->build($currency);
        self::assertInstanceOf(Currency::class, $currency);
    }

    public function buildDataProvider(): array
    {
        return [
           ['EUR'],
           ['USD'],
           ['JPY'],
        ];
    }
}
