<?php

declare(strict_types=1);

namespace App\Tests\Factory;

use App\Factory\User\UserFactory;
use App\Model\User;
use App\Tests\BasePhpUnit;

class UserFactoryTest extends BasePhpUnit
{
    private UserFactory $userFactory;

    public function setUp(): void
    {
        parent::setUp();
        $this->userFactory = new UserFactory();
    }

    /**
     * @dataProvider buildDataProvider
     */
    public function testBuild(
        string $userId,
        string $userType,
    ): void {
        $user = $this->userFactory->build($userId, $userType);
        self::assertInstanceOf(User::class, $user);
    }

    public function buildDataProvider(): array
    {
        return [
           ['1', 'private'],
           ['2', 'business'],
        ];
    }
}
