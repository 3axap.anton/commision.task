<?php

declare(strict_types=1);

namespace App\Tests\Factory;

use App\Factory\Amount\AmountFactory;
use App\Model\Amount;
use App\Model\Currency;
use App\Tests\BasePhpUnit;

class AmountFactoryTest extends BasePhpUnit
{
    private AmountFactory $amountFactory;

    public function setUp(): void
    {
        parent::setUp();
        $this->amountFactory = new AmountFactory();
    }

    /**
     * @dataProvider buildDataProvider
     */
    public function testBuild(
        string $amount,
        Currency $currency,
    ): void {
        $user = $this->amountFactory->build($amount, $currency);
        self::assertInstanceOf(Amount::class, $user);
    }

    /**
     * @dataProvider buildInvalidAmountValueDataProvider
     */
    public function testBuildInvalidUserType(
        string $amount,
        Currency $currency,
    ): void {
        $this->expectException('App\Exception\InvalidAmountValueException');
        $this->amountFactory->build($amount, $currency);
    }

    public function buildDataProvider(): array
    {
        return [
           ['100', new Currency('EUR', '1', 2)],
           ['200', new Currency('USD', '1', 2)],
        ];
    }

    public function buildInvalidAmountValueDataProvider(): array
    {
        return [
            ['abc', new Currency('USD', '1', 2)],
        ];
    }
}
