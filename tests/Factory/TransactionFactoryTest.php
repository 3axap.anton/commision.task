<?php

declare(strict_types=1);

namespace App\Tests\Factory;

use App\Factory\Amount\AmountFactory;
use App\Factory\Currency\CurrencyFactory;
use App\Factory\Transaction\TransactionFactory;
use App\Factory\User\UserFactory;
use App\Model\Operation;
use App\Model\Transaction;
use App\Service\ParseCurrency\ParseCurrencyService;
use App\Tests\BasePhpUnit;

class TransactionFactoryTest extends BasePhpUnit
{
    private TransactionFactory $transactionFactory;

    public function setUp(): void
    {
        parent::setUp();

        $parseCurrencyServiceMock = $this->createMock(ParseCurrencyService::class);
        $parseCurrencyServiceMock->method('getRates')->willReturn([
            'EUR' => '1',
            'USD' => '1.1497',
            'JPY' => '129.53',
        ]);

        $this->transactionFactory = new TransactionFactory(
            'Y-m-d',
            new UserFactory(),
            new CurrencyFactory([
                'EUR' => 2,
                'USD' => 2,
                'JPY' => 0,
            ],
                $parseCurrencyServiceMock
            ),
            new AmountFactory()
        );
    }

    /**
     * @dataProvider buildDataProvider
     */
    public function testBuild(
        Operation $operation
    ): void {
        $transaction = $this->transactionFactory->build($operation);
        self::assertInstanceOf(Transaction::class, $transaction);
    }

    public function buildDataProvider(): array
    {
        return [[
            new Operation('2014-12-31', '4', 'private', 'withdraw', '1200.00', 'EUR'),
            new Operation('2016-01-06', '3', 'business', 'withdraw', '1.00', 'EUR'),
            new Operation('2016-01-05', '1', 'private', 'deposit', '200.00', 'USD'),
        ]];
    }
}
