<?php

declare(strict_types=1);

namespace App\Tests\Factory;

use App\Factory\Amount\AmountFactory;
use App\Factory\Currency\CurrencyFactory;
use App\Factory\Strategy\StrategyFactory;
use App\Service\CurrencyConverter\CurrencyConverterService;
use App\Service\Date\DateService;
use App\Service\Math\MathService;
use App\Service\ParseCurrency\ParseCurrencyService;
use App\Service\Storage\StorageService;
use App\Strategy\Strategies\DepositStrategy;
use App\Strategy\Strategies\StrategyInterface;
use App\Strategy\Strategies\WithdrawBusinessStrategy;
use App\Strategy\Strategies\WithdrawPrivateStrategy;
use App\Tests\BasePhpUnit;

class StrategyFactoryTest extends BasePhpUnit
{
    private StrategyFactory $strategyFactory;

    public function setUp(): void
    {
        parent::setUp();
        $parseCurrencyServiceMock = $this->createMock(ParseCurrencyService::class);
        $parseCurrencyServiceMock->method('getRates')->willReturn([
            'EUR' => '1',
            'USD' => '1.1497',
            'JPY' => '129.53',
        ]);

        $this->strategyFactory = new StrategyFactory(
            new DepositStrategy(
                '0.03',
                new MathService(),
                new AmountFactory()
            ),
            new WithdrawPrivateStrategy(
                'EUR',
                '0.3',
                '1000',
                '3',
                new MathService(),
                new AmountFactory(),
                new CurrencyConverterService(
                    'EUR',
                    new MathService()
                ),
                new CurrencyFactory(
                    [
                        'EUR' => 2,
                        'USD' => 2,
                        'JPY' => 0,
                    ],
                    $parseCurrencyServiceMock
                ),
                new DateService(),
                new StorageService()
            ),
            new WithdrawBusinessStrategy(
                '0.5',
                new MathService(),
                new AmountFactory()
            )
        );
    }

    /**
     * @dataProvider buildDataProvider
     */
    public function testGetStrategy(
        string $strategy
    ): void {
        $strategy = $this->strategyFactory->getStrategy($strategy);
        self::assertInstanceOf(StrategyInterface::class, $strategy);
    }

    /**
     * @dataProvider buildInvalidStrategyDataProvider
     */
    public function testBuildInvalidStrategy(
        string $strategy
    ): void {
        $this->expectException('App\Exception\InvalidStrategyException');
        $this->strategyFactory->getStrategy($strategy);
    }

    public function buildDataProvider(): array
    {
        return [
           [DepositStrategy::class],
           [WithdrawPrivateStrategy::class],
           [WithdrawBusinessStrategy::class],
        ];
    }

    public function buildInvalidStrategyDataProvider(): array
    {
        return [
            ['InvalidClassName'],
        ];
    }
}
