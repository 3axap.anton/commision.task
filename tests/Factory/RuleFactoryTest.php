<?php

declare(strict_types=1);

namespace App\Tests\Factory;

use App\Exception\InvalidRuleException;
use App\Factory\Rule\RuleFactory;
use App\Rules\CurrencyRule;
use App\Rules\DateRule;
use App\Rules\NumericRule;
use App\Rules\RequiredRule;
use App\Rules\RuleInterface;
use App\Rules\TransactionTypeRule;
use App\Rules\UserTypeRule;
use App\Tests\BasePhpUnit;

class RuleFactoryTest extends BasePhpUnit
{
    private RuleFactory $ruleFactory;

    public function setUp(): void
    {
        parent::setUp();
        $this->ruleFactory = new RuleFactory(
            new RequiredRule(),
            new DateRule('Y-m-d'),
            new CurrencyRule([
                'EUR' => 2,
                'USD' => 2,
                'JPY' => 0,
            ]),
            new NumericRule(),
            new TransactionTypeRule(),
            new UserTypeRule()
        );
    }

    /**
     * @dataProvider buildDataProvider
     */
    public function testGetClass(
        string $rule
    ): void {
        $rule = $this->ruleFactory->getClass($rule);
        self::assertInstanceOf(RuleInterface::class, $rule);
    }

    /**
     * @dataProvider buildInvalidRuleDataProvider
     */
    public function testBuildInvalidRule(
        string $rule
    ): void {
        $this->expectException(InvalidRuleException::class);
        $this->ruleFactory->getClass($rule);
    }

    public function buildDataProvider(): array
    {
        return [
           [RequiredRule::class],
           [DateRule::class],
           [CurrencyRule::class],
           [NumericRule::class],
           [TransactionTypeRule::class],
           [UserTypeRule::class],
        ];
    }

    public function buildInvalidRuleDataProvider(): array
    {
        return [
            ['InvalidClassName'],
        ];
    }
}
