<?php

declare(strict_types=1);

namespace App\Tests\Factory;

use App\Factory\Operation\OperationFactory;
use App\Factory\Rule\RuleFactory;
use App\Model\Operation;
use App\Rules\CurrencyRule;
use App\Rules\DateRule;
use App\Rules\NumericRule;
use App\Rules\RequiredRule;
use App\Rules\TransactionTypeRule;
use App\Rules\UserTypeRule;
use App\Tests\BasePhpUnit;
use App\Validator\FileValidator;

class OperationFactoryTest extends BasePhpUnit
{
    private OperationFactory $operationFactory;

    public function setUp(): void
    {
        parent::setUp();
        $fileFields = [
            [
                'key' => 'date',
                'rules' => [
                    RequiredRule::class,
                    DateRule::class,
                ],
            ],
            [
                'key' => 'userId',
                'rules' => [
                    RequiredRule::class,
                    NumericRule::class,
                ],
            ],
            [
                'key' => 'userType',
                'rules' => [
                    RequiredRule::class,
                    UserTypeRule::class,
                ],
            ],
            [
                'key' => 'transactionType',
                'rules' => [
                    RequiredRule::class,
                    TransactionTypeRule::class,
                ],
            ],
            [
                'key' => 'amount',
                'rules' => [
                    RequiredRule::class,
                    NumericRule::class,
                ],
            ],
            [
                'key' => 'currency',
                'rules' => [
                    RequiredRule::class,
                    CurrencyRule::class,
                ],
            ],
        ];
        $this->operationFactory = new OperationFactory(
            new FileValidator(
                $fileFields,
                new RuleFactory(
                    new RequiredRule(),
                    new DateRule('Y-m-d'),
                    new CurrencyRule([
                        'EUR' => 2,
                        'USD' => 2,
                        'JPY' => 0,
                    ]),
                    new NumericRule(),
                    new TransactionTypeRule(),
                    new UserTypeRule()
                ),
            )
        );
    }

    /**
     * @dataProvider buildDataProvider
     */
    public function testBuild(
        $operation
    ): void {
        $operation = $this->operationFactory->build($operation);
        self::assertInstanceOf(Operation::class, $operation);
    }

    public function buildDataProvider(): array
    {
        return [[
            [
                '2014-12-31',
                 '4',
                 'private',
                'withdraw',
                 '1200.00',
                 'EUR',
            ],
            [
                '2016-01-06',
                '3',
                'business',
                'withdraw',
                '1.00',
                'EUR',
            ],
            [
                '2016-01-05',
                '1',
                'private',
                'deposit',
                '200.00',
                'USD',
            ],
        ]];
    }
}
