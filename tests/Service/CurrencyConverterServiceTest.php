<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Model\Currency;
use App\Service\CurrencyConverter\CurrencyConverterService;
use App\Service\Math\MathService;
use App\Tests\BasePhpUnit;

class CurrencyConverterServiceTest extends BasePhpUnit
{
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @dataProvider covertDataProvider
     */
    public function testConvert(
        string $amount,
        Currency $fromCurrency,
        Currency $toCurrency,
        string $defaultCurrency,
        string $expected,
    ): void {
        $service = $this->getCurrencyConverterService($defaultCurrency);
        $this->assertSame($expected, $service->convert($amount, $fromCurrency, $toCurrency));
    }

    private function getCurrencyConverterService(string $defaultCurrency = 'EUR'): CurrencyConverterService
    {
        return new CurrencyConverterService($defaultCurrency, new MathService());
    }

    public function covertDataProvider(): array
    {
        return [
            [
                '100.00',
                new Currency('EUR', '1', 2),
                new Currency('EUR', '1', 2),
                'EUR',
                '100.00',
            ],
            [
                '100.00',
                new Currency('USD', '1', 2),
                new Currency('USD', '1', 2),
                'EUR',
                '100.00',
            ],
            [
                '100.00',
                new Currency('EUR', '1', 2),
                new Currency('USD', '1.1497', 2),
                'EUR',
                '114.97',
            ],
            [
                '100.00',
                new Currency('USD', '1.1497', 2),
                new Currency('EUR', '1', 2),
                'EUR',
                '86.97',
            ],
        ];
    }
}
