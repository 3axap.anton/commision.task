<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Factory\Amount\AmountFactory;
use App\Factory\Currency\CurrencyFactory;
use App\Factory\Strategy\StrategyFactory;
use App\Model\Amount;
use App\Model\Currency;
use App\Model\Transaction;
use App\Model\User;
use App\Service\Commission\CommissionService;
use App\Service\CurrencyConverter\CurrencyConverterService;
use App\Service\Date\DateService;
use App\Service\Math\MathService;
use App\Service\ParseCurrency\ParseCurrencyService;
use App\Service\Storage\StorageService;
use App\Strategy\CommissionStrategy;
use App\Strategy\Strategies\DepositStrategy;
use App\Strategy\Strategies\WithdrawBusinessStrategy;
use App\Strategy\Strategies\WithdrawPrivateStrategy;
use App\Tests\BasePhpUnit;

class CommissionServiceTest extends BasePhpUnit
{
    private CommissionService $commissionService;

    public function setUp(): void
    {
        parent::setUp();

        $parseCurrencyServiceMock = $this->createMock(ParseCurrencyService::class);
        $parseCurrencyServiceMock->method('getRates')->willReturn([
            'EUR' => '1',
            'USD' => '1.1497',
            'JPY' => '129.53',
        ]);

        $this->commissionService = new CommissionService(
            new MathService(),
            new CommissionStrategy(),
            new StrategyFactory(
                new DepositStrategy(
                    '0.03',
                    new MathService(),
                    new AmountFactory()
                ),
                new WithdrawPrivateStrategy(
                    'EUR',
                    '0.3',
                    '1000',
                    '3',
                    new MathService(2),
                    new AmountFactory(),
                    new CurrencyConverterService(
                        'EUR',
                        new MathService()
                    ),
                    new CurrencyFactory(
                        [
                            'EUR' => 2,
                            'USD' => 2,
                            'JPY' => 0,
                        ],
                        $parseCurrencyServiceMock
                    ),
                    new DateService(),
                    new StorageService()
                ),
                new WithdrawBusinessStrategy(
                    '0.5',
                    new MathService(),
                    new AmountFactory()
                )
            )
        );
    }

    /**
     * @dataProvider buildDataProvider
     */
    public function testGetCommission(
        $expected,
        Transaction $transaction
    ): void {
        $amount = $this->commissionService->getCommission($transaction);
        $this->assertSame($expected, $amount);
    }

    public function buildDataProvider(): array
    {
        return [
            [
               '0.60',
               new Transaction(
                   new User(1, 'private'),
                   new Amount('1200', new Currency('EUR', '1', 2)),
                   'withdraw',
                   \DateTime::createFromFormat('Y-m-d', '2014-12-31')
               ),
            ],
            [
                '0.06',
                new Transaction(
                    new User(1, 'private'),
                    new Amount('200', new Currency('EUR', '1', 2)),
                    'deposit',
                    \DateTime::createFromFormat('Y-m-d', '2014-12-31')
                ),
            ],
            [
                '1.50',
                new Transaction(
                    new User(1, 'business'),
                    new Amount('300', new Currency('EUR', '1', 2)),
                    'withdraw',
                    \DateTime::createFromFormat('Y-m-d', '2014-12-31')
                ),
            ],
            [
                '0.60',
                new Transaction(
                    new User(1, 'private'),
                    new Amount('1200', new Currency('USD', '1', 2)),
                    'withdraw',
                    \DateTime::createFromFormat('Y-m-d', '2014-12-31')
                ),
            ],
        ];
    }
}
