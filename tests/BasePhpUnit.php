<?php

declare(strict_types=1);

namespace App\Tests;

use App\Bootstrap\Bootstrap;
use DI\Container;
use PHPUnit\Framework\TestCase;

class BasePhpUnit extends TestCase
{
    protected Container $container;

    public function setUp(): void
    {
        parent::setUp();
        $config = require 'config/config.php';
        $config['ratesApi']['local'] = true;
        $bootstrap = new Bootstrap($config);
        $this->container = $bootstrap->register();
    }
}
