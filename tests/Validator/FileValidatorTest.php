<?php

declare(strict_types=1);

namespace App\Tests\Validator;

use App\Exception\InvalidCurrencyException;
use App\Exception\InvalidDateException;
use App\Exception\InvalidNumericValueException;
use App\Exception\InvalidTransactionTypeException;
use App\Exception\InvalidUserTypeException;
use App\Factory\Rule\RuleFactory;
use App\Rules\CurrencyRule;
use App\Rules\DateRule;
use App\Rules\NumericRule;
use App\Rules\RequiredRule;
use App\Rules\TransactionTypeRule;
use App\Rules\UserTypeRule;
use App\Validator\FileValidator;
use PHPUnit\Framework\TestCase;

class FileValidatorTest extends TestCase
{
    private FileValidator $validator;

    public function setUp(): void
    {
        parent::setUp();
        $fileFields = [
            [
                'key' => 'date',
                'rules' => [
                    RequiredRule::class,
                    DateRule::class,
                ],
            ],
            [
                'key' => 'userId',
                'rules' => [
                    RequiredRule::class,
                    NumericRule::class,
                ],
            ],
            [
                'key' => 'userType',
                'rules' => [
                    RequiredRule::class,
                    UserTypeRule::class,
                ],
            ],
            [
                'key' => 'transactionType',
                'rules' => [
                    RequiredRule::class,
                    TransactionTypeRule::class,
                ],
            ],
            [
                'key' => 'amount',
                'rules' => [
                    RequiredRule::class,
                    NumericRule::class,
                ],
            ],
            [
                'key' => 'currency',
                'rules' => [
                    RequiredRule::class,
                    CurrencyRule::class,
                ],
            ],
        ];
        $this->validator = new FileValidator(
            $fileFields,
                new RuleFactory(
                new RequiredRule(),
                new DateRule('Y-m-d'),
                new CurrencyRule([
                    'EUR' => 2,
                    'USD' => 2,
                    'JPY' => 0,
                ]),
                new NumericRule(),
                new TransactionTypeRule(),
                new UserTypeRule()
            ),
        );
    }

    /**
     * @dataProvider buildDataProvider
     */
    public function testValidate(
        array $row,
        array $expectation
    ): void {
        $operation = $this->validator->validate($row);

        self::assertSame($expectation, $operation);
    }

    /**
     * @dataProvider buildInvalidDateDataProvider
     */
    public function testInvalidDate(
        array $row
    ): void {
        $this->expectException(InvalidDateException::class);
        $this->validator->validate($row);
    }

    /**
     * @dataProvider buildInvalidUserIdDataProvider
     */
    public function testInvalidUserId(
        array $row
    ): void {
        $this->expectException(InvalidNumericValueException::class);
        $this->validator->validate($row);
    }

    /**
     * @dataProvider buildInvalidUserTypeDataProvider
     */
    public function testInvalidUserType(
        array $row
    ): void {
        $this->expectException(InvalidUserTypeException::class);
        $this->validator->validate($row);
    }

    /**
     * @dataProvider buildInvalidTransactionTypeDataProvider
     */
    public function testInvalidTransactionType(
        array $row
    ): void {
        $this->expectException(InvalidTransactionTypeException::class);
        $this->validator->validate($row);
    }

    /**
     * @dataProvider buildInvalidAmountDataProvider
     */
    public function testInvalidAmount(
        array $row
    ): void {
        $this->expectException(InvalidNumericValueException::class);
        $this->validator->validate($row);
    }

    /**
     * @dataProvider buildInvalidCurrencyDataProvider
     */
    public function testInvalidCurrency(
        array $row
    ): void {
        $this->expectException(InvalidCurrencyException::class);
        $this->validator->validate($row);
    }

    public function buildDataProvider(): array
    {
        return [
            'private withdraw EUR' => [
                ['2014-12-31', '4', 'private', 'withdraw', '1200.00', 'EUR'],
                [
                    'date' => '2014-12-31',
                    'userId' => '4',
                    'userType' => 'private',
                    'transactionType' => 'withdraw',
                    'amount' => '1200.00',
                    'currency' => 'EUR',
                ],
            ],
            'private withdraw USD' => [
                ['2014-12-31', '4', 'private', 'withdraw', '1200.00', 'USD'],
                [
                    'date' => '2014-12-31',
                    'userId' => '4',
                    'userType' => 'private',
                    'transactionType' => 'withdraw',
                    'amount' => '1200.00',
                    'currency' => 'USD',
                ],
            ],
            'business withdraw EUR' => [
                ['2014-12-31', '4', 'business', 'withdraw', '1200.00', 'EUR'],
                [
                    'date' => '2014-12-31',
                    'userId' => '4',
                    'userType' => 'business',
                    'transactionType' => 'withdraw',
                    'amount' => '1200.00',
                    'currency' => 'EUR',
                ],
            ],
            'business withdraw USD' => [
                ['2014-12-31', '4', 'business', 'withdraw', '1200.00', 'USD'],
                [
                    'date' => '2014-12-31',
                    'userId' => '4',
                    'userType' => 'business',
                    'transactionType' => 'withdraw',
                    'amount' => '1200.00',
                    'currency' => 'USD',
                ],
            ],
            'business deposit EUR' => [
                ['2014-12-31', '4', 'business', 'deposit', '1200.00', 'EUR'],
                [
                    'date' => '2014-12-31',
                    'userId' => '4',
                    'userType' => 'business',
                    'transactionType' => 'deposit',
                    'amount' => '1200.00',
                    'currency' => 'EUR',
                ],
            ],
            'business deposit USD' => [
                ['2014-12-31', '4', 'business', 'deposit', '1200.00', 'USD'],
                [
                    'date' => '2014-12-31',
                    'userId' => '4',
                    'userType' => 'business',
                    'transactionType' => 'deposit',
                    'amount' => '1200.00',
                    'currency' => 'USD',
                ],
            ],
        ];
    }

    public function buildInvalidDateDataProvider(): array
    {
        return [[
            ['2014-12-311', '4', 'private', 'withdraw', '1200.00', 'EUR'],
        ]];
    }

    public function buildInvalidUserIdDataProvider(): array
    {
        return [[
            ['2014-12-31', '4a', 'private', 'withdraw', '1200.00', 'EUR'],
        ]];
    }

    public function buildInvalidUserTypeDataProvider(): array
    {
        return [[
            ['2014-12-31', '4', 'unique', 'withdraw', '1200.00', 'EUR'],
        ]];
    }

    public function buildInvalidTransactionTypeDataProvider(): array
    {
        return [[
            ['2014-12-31', '4', 'private', 'unique', '1200.00', 'EUR'],
        ]];
    }

    public function buildInvalidAmountDataProvider(): array
    {
        return [[
            ['2014-12-31', '4', 'private', 'withdraw', '1a200.00', 'EUR'],
        ]];
    }

    public function buildInvalidCurrencyDataProvider(): array
    {
        return [[
            ['2014-12-31', '4', 'private', 'withdraw', '1200.00', 'UAHa'],
        ]];
    }
}
