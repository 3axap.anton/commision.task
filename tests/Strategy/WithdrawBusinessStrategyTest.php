<?php

declare(strict_types=1);

namespace App\Tests\Strategy;

use App\Factory\Amount\AmountFactory;
use App\Model\Amount;
use App\Model\Currency;
use App\Model\Transaction;
use App\Model\User;
use App\Service\Math\MathService;
use App\Strategy\Strategies\WithdrawBusinessStrategy;
use App\Tests\BasePhpUnit;

class WithdrawBusinessStrategyTest extends BasePhpUnit
{
    private WithdrawBusinessStrategy $withdrawBusinessStrategy;

    public function setUp(): void
    {
        parent::setUp();
        $this->withdrawBusinessStrategy = new WithdrawBusinessStrategy(
            '0.5',
            new MathService(),
            new AmountFactory()
        );
    }

    /**
     * @dataProvider buildDataProvider
     */
    public function testCalc(
        $expected,
        Transaction $transaction
    ): void {
        $commission = $this->withdrawBusinessStrategy->calc($transaction);
        $this->assertSame($expected, $commission->getValue());
    }

    public function buildDataProvider(): array
    {
        return [
            [
                '6.00',
                new Transaction(
                    new User(1, 'business'),
                    new Amount('1200', new Currency('EUR', '1', 2)),
                    'withdraw',
                    \DateTime::createFromFormat('Y-m-d', '2014-12-31')
                ),
            ],
            [
                '5.00',
                new Transaction(
                    new User(1, 'business'),
                    new Amount('1000', new Currency('USD', '1', 2)),
                    'withdraw',
                    \DateTime::createFromFormat('Y-m-d', '2014-12-31')
                ),
            ],
        ];
    }
}
