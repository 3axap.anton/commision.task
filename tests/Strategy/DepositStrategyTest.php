<?php

declare(strict_types=1);

namespace App\Tests\Strategy;

use App\Factory\Amount\AmountFactory;
use App\Model\Amount;
use App\Model\Currency;
use App\Model\Transaction;
use App\Model\User;
use App\Service\Math\MathService;
use App\Strategy\Strategies\DepositStrategy;
use App\Tests\BasePhpUnit;

class DepositStrategyTest extends BasePhpUnit
{
    private DepositStrategy $depositStrategy;

    public function setUp(): void
    {
        parent::setUp();
        $this->depositStrategy = new DepositStrategy(
            '0.03',
            new MathService(),
            new AmountFactory()
        );
    }

    /**
     * @dataProvider buildDataProvider
     */
    public function testCalc(
        $expected,
        Transaction $transaction
    ): void {
        $commission = $this->depositStrategy->calc($transaction);
        $this->assertSame($expected, $commission->getValue());
    }

    public function buildDataProvider(): array
    {
        return [
            [
                '0.36',
                new Transaction(
                    new User(1, 'private'),
                    new Amount('1200', new Currency('EUR', '1', 2)),
                    'deposit',
                    \DateTime::createFromFormat('Y-m-d', '2014-12-31')
                ),
            ],
            [
                '0.30',
                new Transaction(
                    new User(1, 'private'),
                    new Amount('1000', new Currency('USD', '1', 2)),
                    'deposit',
                    \DateTime::createFromFormat('Y-m-d', '2014-12-31')
                ),
            ],
        ];
    }
}
