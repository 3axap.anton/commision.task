<?php

declare(strict_types=1);

namespace App\Tests\Strategy;

use App\Factory\Amount\AmountFactory;
use App\Factory\Currency\CurrencyFactory;
use App\Model\Amount;
use App\Model\Currency;
use App\Model\Transaction;
use App\Model\User;
use App\Service\CurrencyConverter\CurrencyConverterService;
use App\Service\Date\DateService;
use App\Service\Math\MathService;
use App\Service\ParseCurrency\ParseCurrencyService;
use App\Service\Storage\StorageService;
use App\Strategy\Strategies\WithdrawPrivateStrategy;
use App\Tests\BasePhpUnit;

class WithdrawPrivateStrategyTest extends BasePhpUnit
{
    private WithdrawPrivateStrategy $withdrawPrivateStrategy;

    public function setUp(): void
    {
        parent::setUp();

        $parseCurrencyServiceMock = $this->createMock(ParseCurrencyService::class);
        $parseCurrencyServiceMock->method('getRates')->willReturn([
            'EUR' => '1',
            'USD' => '1.1497',
            'JPY' => '129.53',
        ]);

        $this->withdrawPrivateStrategy = new WithdrawPrivateStrategy(
            'EUR',
            '0.3',
            '1000',
            '3',
            new MathService(),
            new AmountFactory(),
            new CurrencyConverterService(
                'EUR',
                new MathService()
            ),
            new CurrencyFactory(
                [
                    'EUR' => 2,
                    'USD' => 2,
                    'JPY' => 0,
                ],
                $parseCurrencyServiceMock
            ),
            new DateService(),
            new StorageService()
        );
    }

    /**
     * @dataProvider buildDataProvider
     */
    public function testCalc(
        $expected,
        Transaction $transaction
    ): void {
        $commission = $this->withdrawPrivateStrategy->calc($transaction);
        $this->assertSame($expected, $commission->getValue());
    }

    public function buildDataProvider(): array
    {
        return [
            [
                '0.60',
                new Transaction(
                    new User(1, 'private'),
                    new Amount('1200', new Currency('EUR', '1', 2)),
                    'withdraw',
                    \DateTime::createFromFormat('Y-m-d', '2014-12-31')
                ),
            ],
            [
                '0.60',
                new Transaction(
                    new User(1, 'private'),
                    new Amount('1200', new Currency('USD', '1', 2)),
                    'withdraw',
                    \DateTime::createFromFormat('Y-m-d', '2014-12-31')
                ),
            ],
            [
                '0',
                new Transaction(
                    new User(1, 'private'),
                    new Amount('900', new Currency('EUR', '1', 2)),
                    'withdraw',
                    \DateTime::createFromFormat('Y-m-d', '2014-12-31')
                ),
            ],
            [
                '1.50',
                new Transaction(
                    new User(1, 'private'),
                    new Amount('1500', new Currency('USD', '1', 2)),
                    'withdraw',
                    \DateTime::createFromFormat('Y-m-d', '2014-12-31')
                ),
            ],
        ];
    }
}
