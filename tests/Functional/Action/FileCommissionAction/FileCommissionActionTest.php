<?php

declare(strict_types=1);

namespace App\Tests\Functional\Action\FileCommissionAction;

use App\Action\FileCommission\FileCommissionAction;
use App\Service\Input\InputServiceInterface;
use App\Tests\BasePhpUnit;

class FileCommissionActionTest extends BasePhpUnit
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testAction(): void
    {
        $filepath = 'storage/commission.csv';
        $outputString = <<<END
Start
File path - $filepath
0.60
3.00
0.00
0.06
1.50
0
0.70
0.30
0.30
3.00
0.00
0.00
8612
Finish!

END;

        $this->expectOutputString($outputString);
        $service = $this->createStub(InputServiceInterface::class);
        $service->method('getArguments')->willReturn(['', $filepath]);
        $this->container->set(InputServiceInterface::class, $service);
        $output = $this->container->get(FileCommissionAction::class);
        $output->run();
    }

    public function testFailToOpenFile(): void
    {
        $filepath = 'storage/test.csv';
        $outputString = <<<END
Start
File path - $filepath
File does not open
Finish!

END;
        $this->expectOutputString($outputString);

        $service = $this->createMock(InputServiceInterface::class);
        $service->method('getArguments')->willReturn(['', $filepath]);
        $this->container->set(InputServiceInterface::class, $service);
        $output = $this->container->get(FileCommissionAction::class);
        $output->run();
    }
}
