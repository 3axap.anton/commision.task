<?php

require_once __DIR__ . '/vendor/autoload.php';

use App\Bootstrap\Bootstrap;
use App\Action\FileCommission\FileCommissionActionInterface;

$config = require './config/config.php';

$container = (new Bootstrap($config))->register();
$action = $container->get(FileCommissionActionInterface::class);
$action->run();