<?php

use App\Rules\CurrencyRule;
use App\Rules\DateRule;
use App\Rules\NumericRule;
use App\Rules\RequiredRule;
use App\Rules\TransactionTypeRule;
use App\Rules\UserTypeRule;

return [
    'defaultCurrency' => 'EUR',
    'dateFormat' => 'Y-m-d',
    'availableCurrencies' => [
        'EUR' => 2,
        'USD' => 2,
        'JPY' => 0
    ],
    'ratesApi' => [
        'local' => false,
        'ratesApiUrl' => 'http://api.exchangeratesapi.io/v1/latest?access_key=',
        'ratesApiKey' => '43f133c339da8286e93d26d8e75e3e1a',
        'defaultRates' => [
            'EUR' => '1',
            'USD' => '1.1497',
            'JPY' => '129.53'
        ],
    ],
    'commissionDeposit' => '0.03',
    'commissionWithdrawPrivate' => '0.3',
    'commissionWithdrawBusiness' => '0.5',
    'amountFreeCharge' => '1000',
    'countFreeTransactions' => '3',
    'scale' => 8,
    'fileFields' => [
        [
            'key' => 'date',
            'rules' => [
                RequiredRule::class,
                DateRule::class
            ],
        ],
        [
            'key' => 'userId',
            'rules' => [
                RequiredRule::class,
                NumericRule::class
            ],
        ],
        [
            'key' => 'userType',
            'rules' => [
                RequiredRule::class,
                UserTypeRule::class
            ],
        ],
        [
            'key' => 'transactionType',
            'rules' => [
                RequiredRule::class,
                TransactionTypeRule::class
            ],
        ],
        [
            'key' => 'amount',
            'rules' => [
                RequiredRule::class,
                NumericRule::class
            ],
        ],
        [
            'key' => 'currency',
            'rules' => [
                RequiredRule::class,
                CurrencyRule::class
            ],
        ]
    ]
];