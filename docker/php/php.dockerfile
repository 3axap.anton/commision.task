FROM php:8.0-cli-alpine

ADD ./docker/php/www.conf /usr/local/etc/php-fpm.d/

RUN addgroup -g 1000 app && adduser -G app -g app -s /bin/sh -D app

RUN mkdir -p /var/www/html

RUN chown app:app /var/www/html

RUN docker-php-ext-install bcmath
RUN docker-php-ext-configure bcmath --enable-bcmath

USER app

COPY --from=composer /usr/bin/composer /usr/bin/composer

CMD composer install

WORKDIR /var/www/html
