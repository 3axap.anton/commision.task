<?php

declare(strict_types=1);

namespace App\Enum;

final class FileExtension
{
    public const CSV = 'csv';
}
