<?php

declare(strict_types=1);

namespace App\Enum;

final class TransactionTypes
{
    public const DEPOSIT = 'deposit';
    public const WITHDRAW = 'withdraw';
}
