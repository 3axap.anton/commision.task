<?php

declare(strict_types=1);

namespace App\Service\Storage;

use App\Storage\TransactionStorage;
use App\Storage\TransactionStorageInterface;

class StorageService implements StorageServiceInterface
{
    private array $users;

    public function getTransactionStorage(int $userId): TransactionStorageInterface
    {
        if (!isset($this->users[$userId])) {
            $this->users[$userId] = new TransactionStorage();
        }

        return $this->users[$userId];
    }
}
