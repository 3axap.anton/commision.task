<?php

declare(strict_types=1);

namespace App\Service\Storage;

use App\Storage\TransactionStorageInterface;

interface StorageServiceInterface
{
    public function getTransactionStorage(int $userId): TransactionStorageInterface;
}
