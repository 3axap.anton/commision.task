<?php

declare(strict_types=1);

namespace App\Service\ParseCurrency;

interface ParseCurrencyServiceInterface
{
    public function getRates(): array;
}
