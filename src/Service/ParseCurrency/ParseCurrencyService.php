<?php

declare(strict_types=1);

namespace App\Service\ParseCurrency;

use App\Exception\ParseCurrencyException;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Client\ClientInterface;

class ParseCurrencyService implements ParseCurrencyServiceInterface
{
    public function __construct(
        private array $rateApi,
        private ClientInterface $client,
    ) {
    }

    public function getRates(): array
    {
        if (isset($this->rates)) {
            return $this->rates;
        }

        if (!$this->rateApi['local']) {
            try {
                $response = $this->client->sendRequest(new Request(
                    'GET',
                    sprintf('%s%s', $this->rateApi['ratesApiUrl'], $this->rateApi['ratesApiKey'])));

                $data = json_decode((string) $response->getBody(), true);

                $this->rates = $data['rates'] ?? [];

                if (empty($this->rates)) {
                    throw new ParseCurrencyException('Rates array is empty');
                }
            } catch (\Throwable $e) {
                throw new ParseCurrencyException('Error while parse rates API');
            }
        } else {
            $this->rates = $this->rateApi['defaultRates'];
        }

        return $this->rates;
    }
}
