<?php

declare(strict_types=1);

namespace App\Service\Date;

use App\Model\Transaction;

final class DateService implements DateServiceInterface
{
    public function getPeriodDateByTransaction(Transaction $transaction): string
    {
        $date = $transaction->getDate();

        return sprintf(
            '%s-%s',
            $date->modify('this week')->format('Y-m-d'),
            $date->modify('+6 days')->format('Y-m-d')
        );
    }
}
