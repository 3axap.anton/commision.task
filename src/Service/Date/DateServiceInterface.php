<?php

declare(strict_types=1);

namespace App\Service\Date;

use App\Model\Transaction;

interface DateServiceInterface
{
    public function getPeriodDateByTransaction(Transaction $transaction): string;
}
