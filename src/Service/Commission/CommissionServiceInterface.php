<?php

declare(strict_types=1);

namespace App\Service\Commission;

use App\Model\Transaction;

interface CommissionServiceInterface
{
    public function getCommission(Transaction $transaction): string;
}
