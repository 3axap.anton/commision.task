<?php

declare(strict_types=1);

namespace App\Service\Commission;

use App\Enum\TransactionTypes;
use App\Enum\UserTypes;
use App\Factory\Strategy\StrategyFactoryInterface;
use App\Model\Transaction;
use App\Service\Math\MathServiceInterface;
use App\Strategy\CommissionStrategyInterface;
use App\Strategy\Strategies\DepositStrategy;
use App\Strategy\Strategies\WithdrawBusinessStrategy;
use App\Strategy\Strategies\WithdrawPrivateStrategy;

final class CommissionService implements CommissionServiceInterface
{
    public function __construct(
        private MathServiceInterface $mathService,
        private CommissionStrategyInterface $commissionStrategy,
        private StrategyFactoryInterface $strategyFactory
    ) {
    }

    public function getCommission(Transaction $transaction): string
    {
        $strategy = $this->getStrategy($transaction);
        $this->commissionStrategy->setStrategy($strategy);
        $this->commissionStrategy->setTransaction($transaction);
        $amount = $this->commissionStrategy->handle();

        return $this->mathService->roundUp($amount->getValue(), $amount->getCurrency()->getScale());
    }

    private function getStrategy(Transaction $transaction)
    {
        if ($transaction->getTransactionType() === TransactionTypes::DEPOSIT) {
            $strategy = DepositStrategy::class;
        } else {
            $strategy = match ($transaction->getUser()->getUserType()) {
                UserTypes::BUSINESS => WithdrawBusinessStrategy::class,
                UserTypes::PRIVATE => WithdrawPrivateStrategy::class,
            };
        }

        return $this->strategyFactory->getStrategy($strategy);
    }
}
