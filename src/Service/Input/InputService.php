<?php

declare(strict_types=1);

namespace App\Service\Input;

final class InputService implements InputServiceInterface
{
    public function getArguments(): array
    {
        return $_SERVER['argv'];
    }
}
