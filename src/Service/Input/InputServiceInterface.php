<?php

declare(strict_types=1);

namespace App\Service\Input;

interface InputServiceInterface
{
    public function getArguments(): array;
}
