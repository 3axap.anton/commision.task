<?php

declare(strict_types=1);

namespace App\Service\Math;

class MathService implements MathServiceInterface
{
    public function __construct(
        private int $scale = 2
    ) {
    }

    public function add(string $leftOperand, string $rightOperand, int $scale = null): string
    {
        return bcadd($leftOperand, $rightOperand, $scale ?? $this->scale);
    }

    public function sub(string $leftOperand, string $rightOperand, int $scale = null): string
    {
        return bcsub($leftOperand, $rightOperand, $scale ?? $this->scale);
    }

    public function mul(string $leftOperand, string $rightOperand, int $scale = null): string
    {
        return bcmul($leftOperand, $rightOperand, $scale ?? $this->scale);
    }

    public function div(string $leftOperand, string $rightOperand, int $scale = null): string
    {
        return bcdiv($leftOperand, $rightOperand, $scale ?? $this->scale);
    }

    public function percent(string $fromValue, string $percent, int $scale = null): string
    {
        return $this->div($this->mul($fromValue, $percent, $scale), '100', $scale);
    }

    public function roundUp(string $value, int $scale): string
    {
        $rounder = sprintf('0.%s9', str_repeat('0', $scale));

        return $this->add($value, $rounder, $scale);
    }
}
