<?php

declare(strict_types=1);

namespace App\Service\Math;

interface MathServiceInterface
{
    public function add(string $leftOperand, string $rightOperand): string;

    public function sub(string $leftOperand, string $rightOperand): string;

    public function mul(string $leftOperand, string $rightOperand): string;

    public function div(string $leftOperand, string $rightOperand): string;

    public function percent(string $fromValue, string $percent): string;

    public function roundUp(string $value, int $scale): string;
}
