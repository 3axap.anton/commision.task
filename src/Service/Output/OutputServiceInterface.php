<?php

declare(strict_types=1);

namespace App\Service\Output;

interface OutputServiceInterface
{
    public function print(string $string): void;
}
