<?php

declare(strict_types=1);

namespace App\Service\Output;

final class OutputService implements OutputServiceInterface
{
    public function print(string $string): void
    {
        echo sprintf('%s%s', $string, PHP_EOL);
    }
}
