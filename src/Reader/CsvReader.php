<?php

declare(strict_types=1);

namespace App\Reader;

final class CsvReader extends FileReader
{
    public function read(): \Generator
    {
        while (!feof($this->file)) {
            $line = fgetcsv($this->file);
            if ($line) {
                yield $line;
            }
        }
    }
}
