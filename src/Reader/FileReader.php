<?php

declare(strict_types=1);

namespace App\Reader;

use App\Exception\FileNotOpenException;
use Generator;

abstract class FileReader implements FileReaderInterface
{
    protected $file;

    /**
     * @throws FileNotOpenException
     */
    public function openFile(string $filePath): self
    {
        if (file_exists($filePath)) {
            $this->file = fopen($filePath, 'rb');
            if ($this->file !== false) {
                return $this;
            }
        }
        throw new FileNotOpenException('File does not open');
    }

    public function close()
    {
        fclose($this->file);
    }

    abstract public function read(): Generator;
}
