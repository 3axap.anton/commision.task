<?php

declare(strict_types=1);

namespace App\Reader;

interface FileReaderInterface
{
    public function openFile(string $filePath): self;

    public function close();
}
