<?php

declare(strict_types=1);

namespace App\Validator;

use App\Factory\Rule\RuleFactoryInterface;

final class FileValidator implements FileValidatorInterface
{
    public function __construct(
        private array $fileFields,
        private RuleFactoryInterface $ruleFactory,
    ) {
    }

    public function validate(array $row): array
    {
        $operation = [];
        foreach ($this->fileFields as $key => $field) {
            foreach ($field['rules'] as $rule) {
                $value = $row[$key] ?? null;
                $rule = $this->ruleFactory->getClass($rule);
                $rule->setKey($field['key']);
                $rule->setValue($value);
                if (!$rule->passes()) {
                    $rule->exception();
                }
                $operation[$field['key']] = $value;
            }
        }

        return $operation;
    }
}
