<?php

declare(strict_types=1);

namespace App\Validator;

interface FileValidatorInterface
{
    public function validate(array $row): array;
}
