<?php

declare(strict_types=1);

namespace App\Factory\Strategy;

use App\Strategy\Strategies\StrategyInterface;

interface StrategyFactoryInterface
{
    public function getStrategy(string $strategy): StrategyInterface;
}
