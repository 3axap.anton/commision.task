<?php

declare(strict_types=1);

namespace App\Factory\Strategy;

use App\Exception\InvalidStrategyException;
use App\Strategy\Strategies\DepositStrategy;
use App\Strategy\Strategies\StrategyInterface;
use App\Strategy\Strategies\WithdrawBusinessStrategy;
use App\Strategy\Strategies\WithdrawPrivateStrategy;

final class StrategyFactory implements StrategyFactoryInterface
{
    public function __construct(
        private DepositStrategy $depositStrategy,
        private WithdrawPrivateStrategy $withdrawPrivateStrategy,
        private WithdrawBusinessStrategy $withdrawBusinessStrategy,
    ) {
    }

    public function getStrategy(string $strategy): StrategyInterface
    {
        if (!isset($strategy) || !isset($this->allStrategies()[$strategy])) {
            throw new InvalidStrategyException('Strategy does not exist');
        }

        return $this->allStrategies()[$strategy];
    }

    private function allStrategies(): array
    {
        return [
            DepositStrategy::class => $this->depositStrategy,
            WithdrawPrivateStrategy::class => $this->withdrawPrivateStrategy,
            WithdrawBusinessStrategy::class => $this->withdrawBusinessStrategy,
        ];
    }
}
