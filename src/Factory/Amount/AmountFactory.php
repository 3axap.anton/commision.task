<?php

declare(strict_types=1);

namespace App\Factory\Amount;

use App\Exception\InvalidAmountValueException;
use App\Model\Amount;
use App\Model\Currency;

final class AmountFactory implements AmountFactoryInterface
{
    public function build(string $value, Currency $currency): Amount
    {
        if (!is_numeric($value)) {
            throw new InvalidAmountValueException($value);
        }

        return new Amount($value, $currency);
    }
}
