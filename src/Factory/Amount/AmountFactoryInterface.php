<?php

declare(strict_types=1);

namespace App\Factory\Amount;

use App\Model\Amount;
use App\Model\Currency;

interface AmountFactoryInterface
{
    public function build(string $value, Currency $currency): Amount;
}
