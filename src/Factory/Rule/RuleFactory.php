<?php

declare(strict_types=1);

namespace App\Factory\Rule;

use App\Exception\InvalidRuleException;
use App\Rules\CurrencyRule;
use App\Rules\DateRule;
use App\Rules\NumericRule;
use App\Rules\RequiredRule;
use App\Rules\RuleInterface;
use App\Rules\TransactionTypeRule;
use App\Rules\UserTypeRule;

final class RuleFactory implements RuleFactoryInterface
{
    public function __construct(
        private RequiredRule $requiredRule,
        private DateRule $dateRule,
        private CurrencyRule $currencyRule,
        private NumericRule $integerRule,
        private TransactionTypeRule $transactionTypeRule,
        private UserTypeRule $userTypeRule
    ) {
    }

    private function allRules()
    {
        return [
            RequiredRule::class => $this->requiredRule,
            DateRule::class => $this->dateRule,
            CurrencyRule::class => $this->currencyRule,
            NumericRule::class => $this->integerRule,
            TransactionTypeRule::class => $this->transactionTypeRule,
            UserTypeRule::class => $this->userTypeRule,
        ];
    }

    public function getClass(string $rule): RuleInterface
    {
        if (!isset($rule) || !isset($this->allRules()[$rule])) {
            throw new InvalidRuleException('Rule does not exist');
        }

        return $this->allRules()[$rule];
    }
}
