<?php

declare(strict_types=1);

namespace App\Factory\Rule;

use App\Rules\RuleInterface;

interface RuleFactoryInterface
{
    public function getClass(string $rule): RuleInterface;
}
