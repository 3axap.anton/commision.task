<?php

declare(strict_types=1);

namespace App\Factory\User;

use App\Model\User;

final class UserFactory implements UserFactoryInterface
{
    private array $users = [];

    public function build(string $userId, string $userType): User
    {
        if (!isset($this->users[$userId])) {
            $user = new User((int) $userId, $userType);
            $this->users[$userId] = $user;
        }

        return $this->users[$userId];
    }
}
