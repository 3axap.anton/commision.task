<?php

declare(strict_types=1);

namespace App\Factory\User;

use App\Model\User;

interface UserFactoryInterface
{
    public function build(string $userId, string $userType): User;
}
