<?php

declare(strict_types=1);

namespace App\Factory\Currency;

use App\Exception\ParseCurrencyException;
use App\Model\Currency;
use App\Service\ParseCurrency\ParseCurrencyServiceInterface;

final class CurrencyFactory implements CurrencyFactoryInterface
{
    public function __construct(
        private array $availableCurrencies,
        private ParseCurrencyServiceInterface $parseCurrencyService
    ) {
    }

    public function build(string $currencyCode): Currency
    {
        $rates = $this->parseCurrencyService->getRates();
        $rate = $rates[$currencyCode];
        $scale = $this->availableCurrencies[$currencyCode];

        if (empty($rate)) {
            throw new ParseCurrencyException('Rate not found');
        }

        return new Currency($currencyCode, (string) $rate, $scale);
    }
}
