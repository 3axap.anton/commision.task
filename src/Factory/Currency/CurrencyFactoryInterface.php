<?php

declare(strict_types=1);

namespace App\Factory\Currency;

use App\Model\Currency;

interface CurrencyFactoryInterface
{
    public function build(string $currencyCode): Currency;
}
