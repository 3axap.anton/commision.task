<?php

declare(strict_types=1);

namespace App\Factory\Transaction;

use App\Factory\Amount\AmountFactoryInterface;
use App\Factory\Currency\CurrencyFactoryInterface;
use App\Factory\User\UserFactoryInterface;
use App\Model\Operation;
use App\Model\Transaction;

final class TransactionFactory implements TransactionFactoryInterface
{
    public function __construct(
        private $dateFormat,
        private UserFactoryInterface $userFactory,
        private CurrencyFactoryInterface $currencyFactory,
        private AmountFactoryInterface $amountFactory,
    ) {
    }

    public function build(Operation $operation): Transaction
    {
        $user = $this->userFactory->build($operation->getUserId(), $operation->getuserType());
        $currency = $this->currencyFactory->build($operation->getCurrency());
        $amount = $this->amountFactory->build($operation->getAmount(), $currency);
        $dateTime = \DateTime::createFromFormat($this->dateFormat, $operation->getDate());

        return new Transaction(
            $user,
            $amount,
            $operation->getTransactionType(),
            $dateTime,
        );
    }
}
