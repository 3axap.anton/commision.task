<?php

declare(strict_types=1);

namespace App\Factory\Transaction;

use App\Model\Operation;
use App\Model\Transaction;

interface TransactionFactoryInterface
{
    public function build(Operation $operation): Transaction;
}
