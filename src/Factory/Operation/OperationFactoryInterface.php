<?php

declare(strict_types=1);

namespace App\Factory\Operation;

use App\Model\Operation;

interface OperationFactoryInterface
{
    public function build(array $row): Operation;
}
