<?php

declare(strict_types=1);

namespace App\Factory\Operation;

use App\Model\Operation;
use App\Validator\FileValidatorInterface;

final class OperationFactory implements OperationFactoryInterface
{
    public function __construct(
        private FileValidatorInterface $validator
    ) {
    }

    public function build(array $row): Operation
    {
        $data = $this->validator->validate($row);

        return new Operation(
            $data['date'],
            $data['userId'],
            $data['userType'],
            $data['transactionType'],
            $data['amount'],
            $data['currency']
        );
    }
}
