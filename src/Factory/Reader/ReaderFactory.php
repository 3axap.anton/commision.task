<?php

declare(strict_types=1);

namespace App\Factory\Reader;

use App\Enum\FileExtension;
use App\Exception\InvalidExtensionException;
use App\Reader\CsvReader;
use App\Reader\FileReaderInterface;

final class ReaderFactory implements ReaderFactoryInterface
{
    private CsvReader $csvReader;

    public function __construct(
        CsvReader $csvReader
    ) {
        $this->csvReader = $csvReader;
    }

    public function build(string $filePath): FileReaderInterface
    {
        $pathInfo = pathinfo($filePath);

        if (!isset($pathInfo['extension']) || is_null($pathInfo['extension'])) {
            throw new InvalidExtensionException($filePath, 'Extension does not support');
        }

        switch ($pathInfo['extension']) {
            case FileExtension::CSV:
                return $this->csvReader;
        }
    }
}
