<?php

declare(strict_types=1);

namespace App\Factory\Reader;

use App\Reader\FileReaderInterface;

interface ReaderFactoryInterface
{
    public function build(string $filePath): FileReaderInterface;
}
