<?php

declare(strict_types=1);

namespace App\Strategy\Strategies;

use App\Model\Amount;
use App\Model\Transaction;

interface StrategyInterface
{
    public function calc(Transaction $transaction): Amount;
}
