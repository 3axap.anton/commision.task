<?php

declare(strict_types=1);

namespace App\Strategy\Strategies;

use App\Factory\Amount\AmountFactoryInterface;
use App\Factory\Currency\CurrencyFactoryInterface;
use App\Model\Amount;
use App\Model\Commission;
use App\Model\Currency;
use App\Model\Transaction;
use App\Service\CurrencyConverter\CurrencyConverterServiceInterface;
use App\Service\Date\DateServiceInterface;
use App\Service\Math\MathServiceInterface;
use App\Service\Storage\StorageServiceInterface;
use App\Storage\TransactionStorageInterface;
use Iterator;

final class WithdrawPrivateStrategy implements StrategyInterface
{
    private Iterator $transactionByPeriod;
    private string $datePeriod;
    private Currency $defaultCurrencyProject;
    private TransactionStorageInterface $transactionStorage;

    public function __construct(
        private string $defaultCurrency,
        private string $commissionWithdrawPrivate,
        private string $amountFreeCharge,
        private string $countFreeTransactions,
        private MathServiceInterface $mathService,
        private AmountFactoryInterface $amountFactory,
        private CurrencyConverterServiceInterface $currencyConverterService,
        private CurrencyFactoryInterface $currencyFactory,
        private DateServiceInterface $dateService,
        private StorageServiceInterface $storageService
    ) {
    }

    public function calc(Transaction $transaction): Amount
    {
        $this->defaultCurrencyProject = $this->currencyFactory->build($this->defaultCurrency);

        $this->datePeriod = $this->dateService->getPeriodDateByTransaction($transaction);

        $this->transactionStorage = $this->storageService->getTransactionStorage($transaction->getUser()->getUserId());

        $exceededAmountPreviousTransactions = $this->getExceededAmountPreviousTransactions();

        $this->transactionStorage->attach($transaction, $this->datePeriod);

        if ($this->checkCountExceededTransactions() || $this->checkAmountAllExceededTransactions()) {
            $amount = $this->mathService->add(
                $transaction->getAmount()->getValue(),
                $this->currencyConverterService->convert(
                    $exceededAmountPreviousTransactions,
                    $this->defaultCurrencyProject,
                    $transaction->getAmount()->getCurrency(),
                ),
            );

            $commission = new Commission($this->commissionWithdrawPrivate, $amount, $transaction->getAmount()->getCurrency());

            $amount = $this->mathService->percent(
                $commission->getAmountForCommission(),
                $commission->getCommissionFee()
            );
        } else {
            $amount = '0';
        }

        return $this->amountFactory->build($amount, $transaction->getAmount()->getCurrency());
    }

    private function calcExceededAmount($transactionByPeriod): string
    {
        $sum = '0';

        foreach ($transactionByPeriod as $transaction) {
            $sum = $this->mathService
                ->add(
                    $sum,
                    $this->currencyConverterService->convert(
                        $transaction->getAmount()->getValue(),
                        $transaction->getAmount()->getCurrency(),
                        $this->defaultCurrencyProject
                    ),
                );
        }

        return $this->mathService->sub(
            $sum,
            $this->amountFreeCharge
        );
    }

    private function checkCountExceededTransactions(): bool
    {
        return $this->transactionByPeriod->count() >= $this->countFreeTransactions;
    }

    private function checkAmountAllExceededTransactions(): bool
    {
        $this->transactionByPeriod = $this->transactionStorage->getTransactionsByPeriod($this->datePeriod);
        $exceededAmountAllTransactions = $this->calcExceededAmount($this->transactionByPeriod);

        return $exceededAmountAllTransactions > 0;
    }

    private function getExceededAmountPreviousTransactions(): string
    {
        $this->transactionByPeriod = $this->transactionStorage->getTransactionsByPeriod($this->datePeriod);
        $exceededAmountPreviousTransactions = $this->calcExceededAmount($this->transactionByPeriod);

        return $exceededAmountPreviousTransactions > 0 || $this->checkCountExceededTransactions() ? '0' : $exceededAmountPreviousTransactions;
    }
}
