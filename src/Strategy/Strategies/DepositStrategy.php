<?php

declare(strict_types=1);

namespace App\Strategy\Strategies;

use App\Factory\Amount\AmountFactoryInterface;
use App\Model\Amount;
use App\Model\Commission;
use App\Model\Transaction;
use App\Service\Math\MathServiceInterface;

final class DepositStrategy implements StrategyInterface
{
    public function __construct(
        private string $commissionDeposit,
        private MathServiceInterface $mathService,
        private AmountFactoryInterface $amountFactory
    ) {
    }

    public function calc(Transaction $transaction): Amount
    {
        $amount = $transaction->getAmount();

        $commission = new Commission($this->commissionDeposit, $amount->getValue(), $amount->getCurrency());

        $amount = $this->mathService->percent(
            $commission->getAmountForCommission(),
            $commission->getCommissionFee()
        );

        return $this->amountFactory->build($amount, $commission->getCurrencyForCommission());
    }
}
