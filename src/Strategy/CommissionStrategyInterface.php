<?php

declare(strict_types=1);

namespace App\Strategy;

use App\Model\Amount;
use App\Model\Transaction;
use App\Strategy\Strategies\StrategyInterface;

interface CommissionStrategyInterface
{
    public function setStrategy(StrategyInterface $strategy): void;

    public function setTransaction(Transaction $transaction): void;

    public function handle(): Amount;
}
