<?php

declare(strict_types=1);

namespace App\Strategy;

use App\Model\Amount;
use App\Model\Transaction;
use App\Strategy\Strategies\StrategyInterface;

final class CommissionStrategy implements CommissionStrategyInterface
{
    private StrategyInterface $strategy;
    private Transaction $transaction;

    public function setStrategy(StrategyInterface $strategy): void
    {
        $this->strategy = $strategy;
    }

    public function setTransaction(Transaction $transaction): void
    {
        $this->transaction = $transaction;
    }

    public function handle(): Amount
    {
        return $this->strategy->calc($this->transaction);
    }
}
