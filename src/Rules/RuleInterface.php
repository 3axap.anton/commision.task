<?php

declare(strict_types=1);

namespace App\Rules;

interface RuleInterface
{
    public function setValue(?string $value): void;

    public function getValue(): ?string;

    public function setKey(string $value): void;

    public function getKey(): string;
}
