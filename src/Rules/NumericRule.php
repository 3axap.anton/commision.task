<?php

declare(strict_types=1);

namespace App\Rules;

use App\Exception\InvalidNumericValueException;

class NumericRule extends AbstractRule
{
    public function passes(): bool
    {
        return is_numeric($this->getValue());
    }

    public function exception(): void
    {
        throw new InvalidNumericValueException($this->getValue(), 'Invalid numeric value');
    }
}
