<?php

declare(strict_types=1);

namespace App\Rules;

use App\Exception\InvalidCurrencyException;

class CurrencyRule extends AbstractRule
{
    public function __construct(
        private array $availableCurrencies
    ) {
    }

    public function passes(): bool
    {
        return array_key_exists($this->getValue(), $this->availableCurrencies);
    }

    public function exception(): void
    {
        throw new InvalidCurrencyException($this->getValue(), 'Invalid currency');
    }
}
