<?php

declare(strict_types=1);

namespace App\Rules;

use App\Exception\InvalidRequiredFieldException;

class RequiredRule extends AbstractRule
{
    public function passes(): bool
    {
        return $this->getValue() !== null;
    }

    public function exception(): void
    {
        throw new InvalidRequiredFieldException($this->getKey(), 'Key is required');
    }
}
