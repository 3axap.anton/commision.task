<?php

declare(strict_types=1);

namespace App\Rules;

use App\Exception\InvalidDateException;
use DateTime;

class DateRule extends AbstractRule
{
    public function __construct(
        private string $dateFormat
    ) {
    }

    public function passes(): bool
    {
        return DateTime::createFromFormat(
                $this->dateFormat,
                $this->getValue()
            ) !== false;
    }

    public function exception(): void
    {
        throw new InvalidDateException($this->getValue(), 'Invalid date');
    }
}
