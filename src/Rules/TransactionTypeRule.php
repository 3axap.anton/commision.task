<?php

declare(strict_types=1);

namespace App\Rules;

use App\Enum\TransactionTypes;
use App\Exception\InvalidTransactionTypeException;

class TransactionTypeRule extends AbstractRule
{
    public function passes(): bool
    {
        return in_array($this->getValue(), [
            TransactionTypes::DEPOSIT,
            TransactionTypes::WITHDRAW,
        ], true);
    }

    public function exception(): void
    {
        throw new InvalidTransactionTypeException($this->getValue(), 'Invalid transaction type');
    }
}
