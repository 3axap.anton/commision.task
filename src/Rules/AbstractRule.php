<?php

declare(strict_types=1);

namespace App\Rules;

abstract class AbstractRule implements RuleInterface
{
    protected ?string $value;
    private string $key;

    public function setValue(?string $value): void
    {
        $this->value = $value;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setKey(string $key): void
    {
        $this->key = $key;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    abstract public function passes(): bool;

    abstract public function exception(): void;
}
