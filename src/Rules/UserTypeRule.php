<?php

declare(strict_types=1);

namespace App\Rules;

use App\Enum\UserTypes;
use App\Exception\InvalidUserTypeException;

class UserTypeRule extends AbstractRule
{
    public function passes(): bool
    {
        return in_array($this->getValue(), [
            UserTypes::PRIVATE,
            UserTypes::BUSINESS,
        ], true);
    }

    public function exception(): void
    {
        throw new InvalidUserTypeException($this->getValue(), 'Invalid user type');
    }
}
