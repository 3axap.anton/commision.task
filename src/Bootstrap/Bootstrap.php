<?php

declare(strict_types=1);

namespace App\Bootstrap;

use App\Action\FileCommission\FileCommissionAction;
use App\Action\FileCommission\FileCommissionActionInterface;
use App\Factory\Amount\AmountFactory;
use App\Factory\Amount\AmountFactoryInterface;
use App\Factory\Currency\CurrencyFactory;
use App\Factory\Currency\CurrencyFactoryInterface;
use App\Factory\Operation\OperationFactory;
use App\Factory\Operation\OperationFactoryInterface;
use App\Factory\Reader\ReaderFactory;
use App\Factory\Reader\ReaderFactoryInterface;
use App\Factory\Rule\RuleFactory;
use App\Factory\Rule\RuleFactoryInterface;
use App\Factory\Strategy\StrategyFactory;
use App\Factory\Strategy\StrategyFactoryInterface;
use App\Factory\Transaction\TransactionFactory;
use App\Factory\Transaction\TransactionFactoryInterface;
use App\Factory\User\UserFactory;
use App\Factory\User\UserFactoryInterface;
use App\Reader\CsvReader;
use App\Rules\CurrencyRule;
use App\Rules\DateRule;
use App\Service\Commission\CommissionService;
use App\Service\Commission\CommissionServiceInterface;
use App\Service\CurrencyConverter\CurrencyConverterService;
use App\Service\CurrencyConverter\CurrencyConverterServiceInterface;
use App\Service\Date\DateService;
use App\Service\Date\DateServiceInterface;
use App\Service\Input\InputService;
use App\Service\Input\InputServiceInterface;
use App\Service\Math\MathService;
use App\Service\Math\MathServiceInterface;
use App\Service\Output\OutputService;
use App\Service\Output\OutputServiceInterface;
use App\Service\ParseCurrency\ParseCurrencyService;
use App\Service\ParseCurrency\ParseCurrencyServiceInterface;
use App\Service\Storage\StorageService;
use App\Service\Storage\StorageServiceInterface;
use App\Strategy\CommissionStrategy;
use App\Strategy\CommissionStrategyInterface;
use App\Strategy\Strategies\DepositStrategy;
use App\Strategy\Strategies\WithdrawBusinessStrategy;
use App\Strategy\Strategies\WithdrawPrivateStrategy;
use App\Validator\FileValidator;
use App\Validator\FileValidatorInterface;
use DI\Container;
use DI\ContainerBuilder;
use GuzzleHttp\Client;
use Psr\Http\Client\ClientInterface;

final class Bootstrap
{
    private ContainerBuilder $builder;
    private array $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function register(): Container
    {
        $this->builder = new ContainerBuilder();

        $this->registerActions();
        $this->registerServices();
        $this->registerFactories();
        $this->registerConfig();
        $this->registerStrategy();
        $this->registerValidator();

        return $this->builder->build();
    }

    private function registerActions(): void
    {
        $this->builder->addDefinitions([
            FileCommissionActionInterface::class => \DI\get(FileCommissionAction::class),
        ]);
    }

    private function registerServices(): void
    {
        $this->builder->addDefinitions([
            ClientInterface::class => \DI\get(Client::class),
            InputServiceInterface::class => \DI\get(InputService::class),
            OutputServiceInterface::class => \DI\get(OutputService::class),
            ParseCurrencyServiceInterface::class => \DI\autowire(ParseCurrencyService::class)->constructor(
                \DI\get('ratesApi')
            ),
            CommissionServiceInterface::class => \DI\get(CommissionService::class),
            MathServiceInterface::class => \DI\autowire(MathService::class)->constructor(
                \DI\get('scale')
            ),
            CurrencyConverterServiceInterface::class => \DI\autowire(CurrencyConverterService::class)->constructor(
                \DI\get('defaultCurrency')
            ),
            DateServiceInterface::class => \DI\get(DateService::class),
            StorageServiceInterface::class => \DI\get(StorageService::class),
        ]);
    }

    private function registerFactories()
    {
        $this->builder->addDefinitions([
            ReaderFactoryInterface::class => \DI\autowire(ReaderFactory::class)->constructor(
                \DI\get(CsvReader::class),
            ),
            TransactionFactoryInterface::class => \DI\autowire(TransactionFactory::class)->constructor(
                \DI\get('dateFormat')
            ),
            UserFactoryInterface::class => \DI\get(UserFactory::class),
            CurrencyFactoryInterface::class => \DI\autowire(CurrencyFactory::class)->constructor(
                \DI\get('availableCurrencies')
            ),
            AmountFactoryInterface::class => \DI\get(AmountFactory::class),
            StrategyFactoryInterface::class => \DI\get(StrategyFactory::class),
            RuleFactoryInterface::class => \DI\get(RuleFactory::class),
            OperationFactoryInterface::class => \DI\get(OperationFactory::class),
        ]);
    }

    private function registerConfig()
    {
        $this->builder->addDefinitions($this->config);
    }

    private function registerStrategy()
    {
        $this->builder->addDefinitions([
            CommissionStrategyInterface::class => \DI\get(CommissionStrategy::class),
            DepositStrategy::class => \DI\autowire(DepositStrategy::class)->constructor(
                \DI\get('commissionDeposit')
            ),
            WithdrawPrivateStrategy::class => \DI\autowire(WithdrawPrivateStrategy::class)->constructor(
                \DI\get('defaultCurrency'),
                \DI\get('commissionWithdrawPrivate'),
                \DI\get('amountFreeCharge'),
                \DI\get('countFreeTransactions')
            ),
            WithdrawBusinessStrategy::class => \DI\autowire(WithdrawBusinessStrategy::class)->constructor(
                \DI\get('commissionWithdrawBusiness')
            ),
        ]);
    }

    private function registerValidator()
    {
        $this->builder->addDefinitions([
            FileValidatorInterface::class => \DI\autowire(FileValidator::class)->constructor(
                \DI\get('fileFields')
            ),
            DateRule::class => \DI\autowire(DateRule::class)->constructor(
                \DI\get('dateFormat')
            ),
            CurrencyRule::class => \DI\autowire(CurrencyRule::class)->constructor(
                \DI\get('availableCurrencies')
            ),
        ]);
    }
}
