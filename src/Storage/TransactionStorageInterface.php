<?php

declare(strict_types=1);

namespace App\Storage;

use App\Model\Transaction;

interface TransactionStorageInterface
{
    public function attach(Transaction $transaction, string $key): void;
}
