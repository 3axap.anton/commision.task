<?php

declare(strict_types=1);

namespace App\Storage;

use App\Model\Transaction;
use Iterator;
use SplObjectStorage;

final class TransactionStorage implements TransactionStorageInterface
{
    private SplObjectStorage $storage;

    public function __construct()
    {
        $this->storage = new \SplObjectStorage();
    }

    public function attach(Transaction $transaction, string $key): void
    {
        $this->storage->attach($transaction, $key);
    }

    public function getInfo(): mixed
    {
        return $this->storage->getInfo();
    }

    public function rewind(): void
    {
        $this->storage->rewind();
    }

    public function valid(): bool
    {
        return $this->storage->valid();
    }

    public function next(): void
    {
        $this->storage->next();
    }

    public function current(): object
    {
        return $this->storage->current();
    }

    public function count(): int
    {
        return $this->storage->count();
    }

    public function getTransactionsByPeriod(string $key): Iterator
    {
        $this->rewind();
        $storage = new SplObjectStorage();
        while ($this->valid()) {
            if ($key === $this->getInfo()) {
                $storage->attach($this->current());
            }
            $this->next();
        }

        return $storage;
    }
}
