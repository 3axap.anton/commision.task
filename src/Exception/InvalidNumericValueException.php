<?php

declare(strict_types=1);

namespace App\Exception;

class InvalidNumericValueException extends InvalidValueException
{
}
