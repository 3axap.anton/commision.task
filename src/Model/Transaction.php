<?php

declare(strict_types=1);

namespace App\Model;

final class Transaction
{
    public function __construct(
        private User $user,
        private Amount $amount,
        private string $transactionType,
        private \DateTime $date,
    ) {
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getTransactionType(): string
    {
        return $this->transactionType;
    }

    public function getAmount(): Amount
    {
        return $this->amount;
    }
}
