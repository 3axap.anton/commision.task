<?php

declare(strict_types=1);

namespace App\Model;

final class Operation
{
    public function __construct(
        private string $date,
        private string $userId,
        private string $userType,
        private string $transactionType,
        private string $amount,
        private string $currency,
    ) {
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getUserType(): string
    {
        return $this->userType;
    }

    public function getTransactionType(): string
    {
        return $this->transactionType;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }
}
