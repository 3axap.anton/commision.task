<?php

declare(strict_types=1);

namespace App\Action\FileCommission;

use App\Exception\InvalidValueException;
use App\Factory\Operation\OperationFactoryInterface;
use App\Factory\Reader\ReaderFactoryInterface;
use App\Factory\Transaction\TransactionFactoryInterface;
use App\Service\Commission\CommissionServiceInterface;
use App\Service\Input\InputServiceInterface;
use App\Service\Output\OutputServiceInterface;

final class FileCommissionAction implements FileCommissionActionInterface
{
    public function __construct(
        private InputServiceInterface $inputService,
        private OutputServiceInterface $outputService,
        private TransactionFactoryInterface $transactionFactory,
        private CommissionServiceInterface $commissionService,
        private ReaderFactoryInterface $readerFactory,
        private OperationFactoryInterface $operationFactory,
    ) {
    }

    public function run(): void
    {
        try {
            $this->outputService->print('Start');
            $argv = $this->inputService->getArguments();
            $filePath = $argv[1];
            $this->outputService->print(sprintf('File path - %s', $filePath));
            $file = $this->readerFactory->build($filePath);
            $file->openFile($filePath);

            foreach ($file->read() as $row) {
                try {
                    $operation = $this->operationFactory->build($row);
                    $transaction = $this->transactionFactory->build($operation);
                } catch (InvalidValueException $e) {
                    $this->outputService->print(sprintf('%s: %s', $e->getMessage(), $e->getInvalidValue()));
                    continue;
                }
                $commission = $this->commissionService->getCommission($transaction);
                $this->outputService->print($commission);
            }

            $file->close();
        } catch (\Exception $e) {
            $this->outputService->print($e->getMessage());
        }

        $this->outputService->print('Finish!');
    }
}
