<?php

declare(strict_types=1);

namespace App\Action\FileCommission;

interface FileCommissionActionInterface
{
    public function run(): void;
}
