# Commision.task

# Install
1. Go to application directiry
2. Clone repository - ``git clone https://gitlab.com/3axap.anton/commision.task.git .
   ``
3. Build docker - ``docker-compose up -d``

# Run application
1. Go to in docker container - ``docker-compose run php sh``
2. Run php - ``php index.php storage/commission.csv``

# Additional commands
1. Code Standard fixer - ``docker-compose run php ./bin/php-cs-fixer fix``
2. PhpUnit - ``docker-compose run php ./bin/phpunit``

## Config - config/config.php
>***defaultCurrency*** - Default currency  
 ***dateFormat*** - Default date format  
 ***availableCurrencies*** - List of available currencies and scale,  
 ***ratesApi*** - [
     'local' - TRUE - we calc with default rates; FALSE - get rates from api.exchangeratesapi.io  
     'ratesApiUrl' - 'http://api.exchangeratesapi.io/v1/latest?access_key=',  
     'ratesApiKey' - Api key,  
     'defaultRates' - List of default rates  
 ],   
 ***commissionDeposit*** - Commision for deposit stratefy  
 ***commissionWithdrawPrivate*** - Commision for withdraw private   
 ***commissionWithdrawBusiness*** - Commision for withdraw business   
 ***amountFreeCharge*** - Calculation of commission over the amount
 ***scale*** - Default scale    
 ***fileFields*** - validate rules for commission file